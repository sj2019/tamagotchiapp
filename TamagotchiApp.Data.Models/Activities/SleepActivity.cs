﻿using TamagotchApp.Data.Models.Activities;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models.Activities
{
	class SleepActivity : ActivityBase
	{
		public override bool IsPermanent { get => true; }

		public SleepActivity()
		{
			Name = "Sleeping";
		}

		public override void Effect(Pet pet)
		{
			pet.Tiredness -= 5;
			base.Effect(pet);

			if (pet.Tiredness == 0) {
				// Pet is ready to wake up.
				pet.Effects -= this.Effect;
			}
		}
	}
}
