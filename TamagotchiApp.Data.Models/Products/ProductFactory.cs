﻿using System;
using System.Collections.Generic;
using System.Text;
using TamagotchApp.Data.Models.Products;

namespace TamagotchiApp.Data.Models.Products
{
	public static class ProductFactory
	{
		public static IProduct GetProduct(ProductType type)
		{
			switch (type)
			{
				case ProductType.Cookie:
					return new CookieProduct();
				default:
					throw new InvalidOperationException("Unknown product type.");
			}
		}
	}
}
