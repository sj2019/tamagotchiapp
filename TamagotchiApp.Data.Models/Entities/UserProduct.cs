﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TamagotchiApp.Data.Models.Entities
{
	/// <summary>
	/// Represent many-to-many relationship with Product and User tables.
	/// </summary>
	public class UserProduct
	{
		public int UserId { get; set; }
		public User User { get; set; }

		public int ProductId { get; set; }

		public int Quantity { get; set; }
	}
}
