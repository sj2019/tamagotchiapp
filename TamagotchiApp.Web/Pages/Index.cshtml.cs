﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TamagotchiApp.Data.Sql;
using TamagotchiApp.Web.DTOs;
using TamagotchiApp.Web.Services;

namespace TamagotchiApp.Web.Pages
{
	[BindProperties]
	public class IndexModel : PageModel
	{
		private readonly IUserManager _userManager;
		private readonly GameManager _gameManager;
		private readonly GameStorageContext _storageContext;

		public IndexModel(IUserManager userManager, GameManager gameManager, GameStorageContext storageContext)
		{
			_userManager = userManager;
			_gameManager = gameManager;
			_storageContext = storageContext;
			this.Pets = new List<PetInfo>();
		}

		public string UserName { get; set; }

		public List<PetInfo> Pets {get; set;}

		[Required]
		public string PetName { get; set; }

		public void OnGet()
		{
			if (!HttpContext.User.Identity.IsAuthenticated)
			{
				var user = _userManager.CreateDummyUser();
				_userManager.SignIn(HttpContext, user);
				this.UserName = user.Name;
			}
			else {
				this.UserName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;

				var userId = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

				var user = _storageContext.Users.Where(u => u.Id.ToString() == userId).Include(u => u.Pets).FirstOrDefault();

				if (user != null)
				{
					foreach (var pet in user.Pets)
					{
						this.Pets.Add(new PetInfo(pet));
					}
				}
			}
		}

		public IActionResult OnPost()
		{
			this.UserName = HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name).Value;
			var userId = Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
			_gameManager.AddPet(userId, this.PetName);

			return RedirectToPage("./index");
		}
	}
}
