﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TamagotchApp.Data.Models.Managers;
using TamagotchApp.Data.Models.Products;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Activities;
using TamagotchiApp.Data.Models.Entities;
using TamagotchiApp.Data.Models.Products;
using TamagotchiApp.Data.Sql;
using TamagotchiApp.Web;
using TamagotchiApp.Web.DTOs;

namespace TamagotchiApp.Web.Services
{
	public class GameManager
	{
		private ConcurrentDictionary<int, Pet> _pets;
		private ConcurrentDictionary<int, User> _users;
		private ConcurrentDictionary<string, UserProduct> _userProducts;

		private readonly IServiceScopeFactory _serviceFactory;
		private readonly IHubContext<GameHub> _hubContext;

		public GameManager(IServiceScopeFactory serviceFactory, IHubContext<GameHub> hubContext)
		{
			_serviceFactory = serviceFactory;
			_hubContext = hubContext;

			_pets = new ConcurrentDictionary<int, Pet>();
			_users = new ConcurrentDictionary<int, User>();
			_userProducts = new ConcurrentDictionary<string, UserProduct>();

			Initialize();
		}

		private void Initialize()
		{
			// Populates shared collections with data from DB.
			using (var serviceScope = _serviceFactory.CreateScope())
			{
				using (var storageCtx = serviceScope.ServiceProvider.GetRequiredService<GameStorageContext>())
				{
					var petsCollection = storageCtx.Pets.ToList();
					foreach (var pet in petsCollection)
					{
						PetManager.ApllyGeneralEffects(pet);
						_pets.AddOrUpdate(pet.Id, (k) => pet, (key, value) => pet);
					}

					var usersCollection = storageCtx.Users.ToList();
					foreach (var user in usersCollection)
					{
						_users.AddOrUpdate(user.Id, (k) => user, (key, value) => user);
					}

					var userProducts = storageCtx.UserProducts.ToList();
					foreach (var up in userProducts)
					{
						_userProducts.AddOrUpdate($"{up.UserId}^{up.ProductId}", (k) => up, (key, value) => up);
					}
				}
			}
		}

		public IList<Pet> ApplyGameCycle()
		{
			var pets = _pets.Select(pair => pair.Value);
			foreach (var pet in pets)
			{
				if (pet.IsAlive)
				{
					pet.Effects.Invoke(pet);

					int userId = Convert.ToInt32(pet.UserId);
					_hubContext.Clients.User(pet.UserId.ToString()).SendAsync("RecievePet", new PetInfo(pet));
				}
			}
			return pets.ToList();
		}

		public void AddProduct(UserProduct userProduct)
		{
			using (var serviceScope = _serviceFactory.CreateScope())
			{
				using (var storageCtx = serviceScope.ServiceProvider.GetRequiredService<GameStorageContext>())
				{
					storageCtx.UserProducts.Add(userProduct);
					storageCtx.SaveChanges();

					// Adds to shared collection too. Pet id should be already populated after SaveChanges executed.
					_userProducts.AddOrUpdate($"{userProduct.UserId}^{userProduct.ProductId}", (k) => userProduct, (key, value) => userProduct);
				}
			}
		}

		public void AddPet(int userId, string name)
		{
			// Adds a pet to the database.
			using (var serviceScope = _serviceFactory.CreateScope())
			{
				using (var storageCtx = serviceScope.ServiceProvider.GetRequiredService<GameStorageContext>())
				{
					Pet pet = new Pet(name);
					PetManager.ApllyGeneralEffects(pet);

					pet.UserId = userId;
					storageCtx.Pets.Add(pet);
					storageCtx.SaveChanges();

					// Adds to shared collection too. Pet id should be already populated after SaveChanges executed.
					_pets.AddOrUpdate(pet.Id, (k) => pet, (key, value) => pet);
				}
			}
		}

		public void FeedPet(int userId, int petId, int productId)
		{
			Pet pet = this.GetPet(petId);

			UserProduct up;
			string key = $"{userId}^{productId}";
			if (_userProducts.TryGetValue(key, out up))
			{
				if (up.Quantity > 0)
				{
					// Curently we assume that product id is defined in the enum below.
					ProductType productType = (ProductType)up.ProductId;
					var product = ProductFactory.GetProduct(productType);
					PetManager.Feed(pet, product);

					up.Quantity--;

					// Removes record from collection 
					if (up.Quantity == 0)
					{
						_userProducts.TryRemove(key, out _);
					}
				}
				else
				{
					throw new GameException($"User ran out of product with id: {up.ProductId}");
				}

			}
			else
			{
				throw new GameException($"There is no pet with id: {petId}");
			}
		}

		public void DoActivity(int petId, int activityId)
		{
			Pet pet = this.GetPet(petId);
			ActivityType activityType = (ActivityType)activityId;
				var activity = ActivityFactory.GetActivity(activityType);
				PetManager.DoActivity(pet, activity);
		}

		public Pet GetPet(int petId)
		{
			Pet pet;
			if (_pets.TryGetValue(petId, out pet))
			{
				return pet;
			}
			else
			{
				throw new GameException($"There is no pet with id: {petId}");
			}
		}

		/// <summary>
		/// Syncronization of changes from shared collections to database.
		/// </summary>
		/// <returns>
		/// A task that represents the asynchronous save operation.
		/// </returns>
		public async Task SaveChangesAsync()
		{
			using (var serviceScope = _serviceFactory.CreateScope())
			{
				using (var storageCtx = serviceScope.ServiceProvider.GetRequiredService<GameStorageContext>())
				{
					var pets = _pets.Select(pair => pair.Value);
					storageCtx.UpdateRange(pets);

					var users = _users.Select(pair => pair.Value);
					storageCtx.UpdateRange(users);

					var userProducts = _userProducts.Select(pair => pair.Value);
					storageCtx.UpdateRange(users);

					await storageCtx.SaveChangesAsync();
				}
			}
		}

	}
}
