﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Web.DTOs
{
	public class PetInfo
	{
		public PetInfo(Pet pet)
		{
			Id = pet.Id;
			Name = pet.Name;
			IsAlive = pet.IsAlive;
			Sadness = pet.Sadness;
			Fullness = pet.Fullness;
			Tiredness = pet.Tiredness;
			Hangriness = pet.Hangriness;
		}

		public int Id { get; set; }

		public string Name {get; set;}

		public bool IsAlive { get; set; }

		public int Sadness { get; set; }

		public int Fullness { get; set; }

		public int Tiredness { get; set; }

		public int Hangriness { get; set; }
	}
}
