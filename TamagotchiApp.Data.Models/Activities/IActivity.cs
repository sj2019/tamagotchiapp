﻿using System;
using System.Collections.Generic;
using System.Text;
using TamagotchApp.Data.Models.Effects;

namespace TamagotchApp.Data.Models.Activities
{
	public interface IActivity : IEffect
	{
		public string Name { get; set; }

		public bool IsIntermittent { get; set; }
	}
}
