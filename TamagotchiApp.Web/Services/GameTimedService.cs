﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Web.Services
{
	public class GameTimedService: IHostedService, IDisposable
	{
		private Timer _timer;
		private Timer _syncTimer;

		private readonly GameManager _gameManager;

		/// <summary>
		/// The time interval in seconds for changing pets needs.
		/// </summary>
		public int TimerInterval { get; set; }

		/// <summary>
		/// The time intercal in seconds for syncronization changes to the database.
		/// </summary>
		public int SynchronizationTimerInterval { get; set; }
		

		public GameTimedService(GameManager gameManager, IConfiguration configuration)
		{
			_gameManager = gameManager;

			TimerInterval = configuration.GetSection("GameService").GetValue<int>("TimerInterval");
			SynchronizationTimerInterval = configuration.GetSection("GameService").GetValue<int>("SynchronizationTimerInterval");

		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			var timerTimeStamp = TimeSpan.FromSeconds(TimerInterval);
			_timer = new Timer(DoWork, null, timerTimeStamp, timerTimeStamp);


			var syncTimerTimeStamp = TimeSpan.FromSeconds(SynchronizationTimerInterval);
			_syncTimer = new Timer(DoSynchronization, null, syncTimerTimeStamp, syncTimerTimeStamp);

			return Task.CompletedTask;
		}

		private void DoWork(object state)
		{
			var pets = _gameManager.ApplyGameCycle();
		}

		private void DoSynchronization(object state)
		{ 
			_gameManager.SaveChangesAsync().Wait();
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_timer?.Change(Timeout.Infinite, 0);

			// Ensure that all changes will be in the database.
			return _gameManager.SaveChangesAsync();
		}

		public void Dispose()
		{
			_timer.Dispose();
			_syncTimer.Dispose();
		}
	}
}
