﻿using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models.Effects
{
	/// <summary>
	/// The effect that represent the change for any pet over time.
	/// </summary>
	public class TimeEffect: EffectBase
	{
		public TimeEffect() {
			IsPermanent = true;
		}

		public override void Effect(Pet pet)
		{
			pet.MakeOlder();

			pet.Tiredness++;
			pet.Hangriness++;
			pet.Sadness++;

			base.Effect(pet);
		}
	}
}
