﻿using TamagotchApp.Data.Models.Activities;
using TamagotchApp.Data.Models.Products;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Effects;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchApp.Data.Models.Managers
{
	public static class PetManager
	{
		public static void ApllyGeneralEffects(Pet pet)
		{
			if (pet.IsAlive)
			{
				pet.Effects += new TimeEffect().Effect;
			}
		}

		public static void Feed(Pet pet, IProduct product)
		{
			if (pet.Hangriness == 0)
			{
				throw new PetException("Cannot feed the pet because it is not hungry.", pet);
			}

			var feedActivity = new FeedActivity();
			feedActivity.Product = product;

			DoActivity(pet, feedActivity);
		}

		public static void DoActivity(Pet pet, IActivity activity)
		{
			if (pet.CurrentActivity != null && !pet.CurrentActivity.IsIntermittent)
			{
				throw new PetException("Cannot do the activity because it is doing something which cannot be interrupted.", pet);
			}
			else
			{
				if (pet.CurrentActivity != null)
				{
					pet.Effects -= pet.CurrentActivity.Effect;
				}

				pet.Effects += activity.Effect;
			}
		}
	}
}
