﻿using System;
using System.Collections.Generic;
using System.Text;
using TamagotchApp.Data.Models.Activities;
using TamagotchApp.Data.Models.Products;

namespace TamagotchiApp.Data.Models.Activities
{
	public static class ActivityFactory
	{
		public static IActivity GetActivity(ActivityType type)
		{
			switch (type)
			{
				case ActivityType.Sleep:
					return new SleepActivity();
				case ActivityType.Play:
					return new PlayActivity();
				case ActivityType.Poop:
					return new PoopActivity();
				default:
					throw new InvalidOperationException("Unknown product type.");
			}
		}
	}
}
