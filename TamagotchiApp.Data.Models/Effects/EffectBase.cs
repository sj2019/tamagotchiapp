﻿using System;
using TamagotchApp.Data.Models.Effects;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models.Effects
{
	public abstract class EffectBase: IEffect
	{
		private int _duration;

		public virtual bool IsPermanent { get; set; } = false;
		
		public int Times { get; private set; }

		public int Duration
		{
			get => _duration;
			set
			{
				if (value < 1 && !this.IsPermanent)
				{
					throw new InvalidOperationException("The duration should be more than 0.");
				}
				_duration = value;
				Times = value;
			}
		}

		public virtual void Effect(Pet pet) {
			if (!this.IsPermanent)
			{
				this.Times--;

				if (this.Times == 0)
				{
					pet.Effects -= this.Effect;
				}
			}
		}
	}
}
