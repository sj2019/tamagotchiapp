﻿using System;
using TamagotchApp.Data.Models.Activities;

using TamagotchApp.Data.Models.Products;

namespace TamagotchiApp.Data.Models.Entities
{
	public delegate void PetEffects(Pet pet);

	public class Pet
	{
		// The total amount of game cycles after which pet will die.
		private int _lifeSpan;

		public Pet()
		{
			Created = DateTime.Now;
			// Only God knows about its life expectancy.
			Random random = new Random();
			_lifeSpan = random.Next(50, 100);
		}

		public void MakeOlder() {
			_lifeSpan--;
		}

		public IActivity CurrentActivity
		{
			get
			{
				Type effectType;
				if (this.Effects != null)
				{
					foreach (var effect in this.Effects.GetInvocationList())
					{
						effectType = effect.GetType();
						if (effectType.IsSubclassOf(typeof(ActivityBase)) ||
							// ProductBase represents FeedActivity in tho case.
							effectType.IsSubclassOf(typeof(ProductBase)))
						{
							return (effect as IActivity);
						}
					}
				}
				return null;
			}
		}

		public PetEffects Effects { get; set; }

		public int Id { get; set; }

		/// <summary>
		/// The name of the pet.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// The date when the user was created.
		/// </summary>
		public DateTime Created { get; }

		protected void AutoPoop(int fulnessDelta = 10)
		{
			this.Fullness -= fulnessDelta;

			// The pet gets upset when it craps its pants.
			this.Sadness += 10;
		}

		public Pet(string name) : this()
		{
			Name = name;
		}

		/// <summary>
		/// Pet can die of starvation or ageing.
		/// </summary>
		public bool IsAlive { get => Hangriness < 100 && _lifeSpan > 0; }

		/// <summary>
		/// Pet's owner id.
		/// </summary>
		public int UserId { get; set; }

		/// <summary>
		/// Pet's owner.
		/// </summary>
		public User User { get; set; }

		private int adjustNeedBoundary(int value)
		{
			if (value < 0) value = 0;
			if (value > 100) value = 100;
			return value;
		}

		private int _hangriness;
		public int Hangriness
		{
			get => _hangriness;
			set
			{
				_hangriness = this.adjustNeedBoundary(value);
			}
		}

		private int _fullness;
		public int Fullness
		{
			get => _fullness;
			set
			{
				_fullness = this.adjustNeedBoundary(value);
				if (_fullness == 100)
				{
					this.AutoPoop();
				}
			}
		}

		private int _tiredness;
		public int Tiredness
		{
			get => _tiredness;
			set
			{
				_tiredness = this.adjustNeedBoundary(value);
			}
		}

		private int _sadness;
		public int Sadness
		{
			get => _sadness;
			set
			{
				_sadness = this.adjustNeedBoundary(value);
			}
		}
	}
}
