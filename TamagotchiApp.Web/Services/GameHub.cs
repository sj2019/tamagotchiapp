﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Activities;
using TamagotchiApp.Data.Models.Products;

namespace TamagotchiApp.Web.Services
{
	//[Authorize]
	public class GameHub: Hub
	{
		public override Task OnConnectedAsync()
		{
			return base.OnConnectedAsync();
		}

		public override Task OnDisconnectedAsync(Exception exception)
		{
			return base.OnDisconnectedAsync(exception);
		}

		private readonly GameManager _gameManager;
		public GameHub(GameManager gameManager)
		{
			_gameManager = gameManager;
		}

		public async Task DoAction(string action, string petId)
		{
			var userId = Convert.ToInt32(Context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value);
			int id = Convert.ToInt32(petId);

			try
			{
				switch (action)
				{
					case "play":
						_gameManager.DoActivity(id, (int)ActivityType.Play);
						break;
					case "feed":
						_gameManager.FeedPet(userId, id, (int)ProductType.Cookie);
						break;
					case "sleep":
						_gameManager.DoActivity(id, (int)ActivityType.Sleep);
						break;
					case "poop":
						_gameManager.DoActivity(id, (int)ActivityType.Poop);
						break;
				}
			}
			catch (Exception ex)
			{
				if (ex is PetException || ex is GameException)
				{
					await Clients.User(userId.ToString()).SendAsync("NotificationRecieved", ex.Message);
				}
				else
				{
					throw;
				}
			}

			await Clients.User(userId.ToString()).SendAsync("ActionReceived");
		}
	}
}
