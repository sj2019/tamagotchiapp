﻿using System;
using TamagotchApp.Data.Models.Products;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchApp.Data.Models.Activities
{
	class FeedActivity: ActivityBase
	{
		public IProduct Product { get; set; }

		public FeedActivity()
		{
			Name = "Feeding";
		}

		public override void Effect(Pet pet)
		{
			if (this.Product != null)
			{
				this.Product.Effect(pet);
			}
			else
			{
				throw new InvalidOperationException("Cannot perform the feeding because product is not set");
			}
		}
	}
}
