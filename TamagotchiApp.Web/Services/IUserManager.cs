﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Web.Services
{
	public interface IUserManager
	{
		void SignIn(HttpContext httpContext, User user = null, bool isPersistent = false);
		public User CreateDummyUser();
	}
}
