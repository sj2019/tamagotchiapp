﻿$(init);
const connection = new signalR.HubConnectionBuilder()
	.withUrl("/hub")
	.configureLogging(signalR.LogLevel.Information)
	.build();

var petId = getQueryParam("id");

function init() {
	stats = {};
	$('.stat-container > label').each(function () {
		var label = $(this);
		var name = label.text().trim().toLowerCase();
		name = name.substring(0, name.length - 1);
		var progress = label.next().children().first();
		progress.width(0);
		stats[name] = {
			value: progress.width(),
			name,
			progress
		};
	});

	connection.on("RecievePet", function (s) {
		UpdatePet(s);
	});

	connection.on("ActionReceived", function ()
	{
		
	});

	connection.on("NotificationRecieved", function (text)
	{
		toastr.info(text, "", { timeOut: 3000 });
	});

	connection.start().catch(function (err) {
		return console.error(err.toString());
	});

	$('.action-button').each(function (btn) {
		var a = $(this);
		a.on('click', function (e) {
			e.preventDefault();
			connection.invoke("DoAction", a.data('action'), petId).catch(function (err) {
				return console.error(err.toString());
			});
		});
	});
}

function GetStats() {
	connection.invoke("GetStats").then(s => console.log('conn started')).catch(function (err) {
		return console.error(err.toString());
	});
}

function UpdatePet(pet) {

	if (petId === pet.id.toString()) {
		var updateProgress = function (p) {
			p.removeClass('bg-success bg-warning bg-danger bg-info bg-deafult');
			p.width(w + '%');
			var cls;
			if (w > 75) {
				cls = 'bg-success';
			} else if (w > 50) {
				cls = 'bg-info';
			} else if (w > 25) {
				cls = 'bg-warning';
			} else {
				cls = 'bg-danger';
			}
			p.addClass(cls);
		};

		for (var name in pet) {
			if (['sadness', 'tiredness', 'fullness', 'hangriness'].indexOf(name) !== -1) {
				stats[name].value = pet[name];
				var w = pet[name];
				var p = stats[name].progress;
				updateProgress(p);
			}
		}
	}
}