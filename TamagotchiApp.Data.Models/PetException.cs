﻿using System;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models
{
	[Serializable]
	public class PetException: Exception
	{
		public PetException(string message, Pet pet)
	 : base(message)
		{
			Pet = pet;
		}

		public Pet Pet { get; private set; }
	}
}
