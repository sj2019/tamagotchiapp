﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using TamagotchiApp.Data.Sql;
using TamagotchiApp.Data.Models.Entities;
using TamagotchiApp.Data.Models.Products;

namespace TamagotchiApp.Web.Services
{
	public class UserManager : IUserManager
	{
		private readonly GameStorageContext _storageContext;
		private readonly GameManager _gameManager;

		public UserManager(GameStorageContext storage, GameManager gameManager)
		{
			this._gameManager = gameManager;
			this._storageContext = storage;
		}

		private IEnumerable<Claim> GetUserClaims(User user)
		{
			List<Claim> claims = new List<Claim>();

			claims.Add(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
			claims.Add(new Claim(ClaimTypes.Name, user.Name));
			return claims;
		}

		public User CreateDummyUser()
		{
			var totalUsers = this._storageContext.Users.Count();
			return new User { Name = $"User {totalUsers + 1}" };
		}

		public async void SignIn(HttpContext httpContext, User user = null, bool isPersistent = false)
		{
			if (user == null)
			{
				user = this.CreateDummyUser();
			}

			this._storageContext.Users.Add(user);
			this._storageContext.SaveChanges();


			// Adds almost infinite number of cookies for the user. For demostration perpose only.
			this._gameManager.AddProduct(new UserProduct { UserId = user.Id, ProductId = (int)ProductType.Cookie, Quantity = 900000000 });

			this._storageContext.SaveChanges();

			ClaimsIdentity identity = new ClaimsIdentity(this.GetUserClaims(user), CookieAuthenticationDefaults.AuthenticationScheme);
			ClaimsPrincipal principal = new ClaimsPrincipal(identity);

			await httpContext.SignInAsync(
			  CookieAuthenticationDefaults.AuthenticationScheme, principal, new AuthenticationProperties() { IsPersistent = isPersistent }
			);
		}
	}
}
