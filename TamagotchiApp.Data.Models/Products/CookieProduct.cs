﻿using TamagotchiApp.Data.Models.Entities;

namespace TamagotchApp.Data.Models.Products
{
	public class CookieProduct: ProductBase
	{
		public CookieProduct()
		{
			Id = 0;
			Name = "Tasty Cookie";
			Cost = 10.25m;
			Duration = 1;
		}

		public override void Effect(Pet pet)
		{
			pet.Hangriness -= 5;
			pet.Fullness += 5;
			base.Effect(pet);
		}
	}
}
