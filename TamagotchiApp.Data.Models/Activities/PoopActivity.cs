﻿using TamagotchApp.Data.Models.Activities;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models.Activities
{
	class PoopActivity: ActivityBase
	{
		public PoopActivity()
		{
			Name = "Poop";
			Duration = 3;
		}

		public override bool IsIntermittent { get => true; }

		public override void Effect(Pet pet)
		{
			pet.Fullness -= 5;
			base.Effect(pet);

			if (pet.Fullness == 0)
			{
				// Nothing to do in the toilet.
				pet.Effects -= this.Effect;
			}
		}
	}
}
