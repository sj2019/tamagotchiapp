using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using TamagotchiApp.Web.DTOs;
using TamagotchiApp.Web.Services;

namespace TamagotchiApp.Web.Pages
{
	[BindProperties]
	public class ViewPetModel : PageModel
	{
		private readonly GameManager _gameManager;

		public ViewPetModel(GameManager gameManager)
		{
			_gameManager = gameManager;
		}

		public PetInfo Info { get; set; }
		public void OnGet(int id)
		{
			var pet = _gameManager.GetPet(id);
			this.Info = new PetInfo(pet);
		}
	}
}
