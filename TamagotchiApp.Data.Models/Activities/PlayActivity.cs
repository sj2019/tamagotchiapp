﻿using TamagotchiApp.Data.Models.Entities;

namespace TamagotchApp.Data.Models.Activities
{
	class PlayActivity: ActivityBase
	{
		public PlayActivity()
		{
			Name = "Simple Play";
			Duration = 3;
		}

		public override void Effect(Pet pet)
		{
			pet.Tiredness += 5;
			pet.Sadness -= 5;
			base.Effect(pet);
		}
	}
}
