# Tamagotchi Demo!

The prototype of real time game where a user is responsible for looking after his pets.
Users are able to create one or more pets and change their needs in order to help them grow.

The solution contains 3 separate projects:

 - TamagotchiApp.Data.Models - represents main entities and business logic
 - TamagotchiApp.Data.Sql - represents database context based on EF Core and MS SQL
 - TamagotchiApp.Web - web application that represents UI and hosted environment (ASP.Net Core 3 & SignalR Core)