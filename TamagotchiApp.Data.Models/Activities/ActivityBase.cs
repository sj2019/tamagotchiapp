﻿using System;
using System.Collections.Generic;
using System.Text;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Effects;

namespace TamagotchApp.Data.Models.Activities
{
	public abstract class ActivityBase: EffectBase, IActivity
	{ 
		public string Name { get; set; }

		public virtual bool IsIntermittent { get; set; } = true;

		public ActivityBase() {
		}
	}
}
