﻿using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchApp.Data.Models.Effects
{
	/// <summary>
	/// Represents general effect to a pet.
	/// </summary>
	public interface IEffect
	{
		/// <summary>
		/// Indicates whether the effect has unlimited numbers of invocation.
		/// </summary>
		public bool IsPermanent { get; set; }

		/// <summary>
		/// The amount of game cycles in which the effect will influenc to a pet.
		/// </summary>
		public int Duration { get; set; }

		/// <summary>
		/// The logic of the effect respectively.
		/// </summary>
		/// <param name="pet"></param>
		public virtual void Effect(Pet pet) { }
	}
}
