﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TamagotchiApp.Web.Services;

namespace TamagotchiApp.Web.Controllers
{
	[Authorize]
	public class UserController : Controller
    {

		private IUserManager userManager;

		public UserController(IUserManager userManager)
		{
			this.userManager = userManager;
		}

		[Route("/login")]
		[AllowAnonymous]
		public IActionResult Index()
        {

			if (!HttpContext.User.Identity.IsAuthenticated)
			{
				userManager.SignIn(HttpContext);
			}

			return Redirect("/");
        }
    }
}