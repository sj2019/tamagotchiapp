﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Effects;

namespace TamagotchApp.Data.Models.Products
{
	public abstract class ProductBase : EffectBase, IProduct
	{
		public int Id { get; set; }

		public decimal Cost { get; set; }

		public string Name { get; set; }

		public ProductBase()
		{ }
	}
}
