﻿using System;
using System.Collections.Generic;
namespace TamagotchiApp.Data.Models.Entities
{
	public class User
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public DateTime Created { get; }

		public decimal Balance { get; set; }

		public string Email { get; set; }

		/// <summary>
		/// The list of user's pets.
		/// </summary>
		public List<Pet> Pets { get; set; }

		/// <summary>
		/// The collection of products which user bought from the "Tamagotchi Store"
		/// </summary>
		public List<UserProduct> UserProducts { get; set; }

		public User()
		{
			this.Created = DateTime.Now;
			this.Pets = new List<Pet>();
			this.UserProducts = new List<UserProduct>();
		}
	}
}
