﻿using Microsoft.EntityFrameworkCore;
using System;
using TamagotchiApp.Data.Models;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Sql
{
	public class GameStorageContext : DbContext
	{
		public DbSet<User> Users { get; set; }
		public DbSet<Pet> Pets { get; set; }
		public DbSet<UserProduct> UserProducts { get; set; }

		public GameStorageContext() { }

		public GameStorageContext(DbContextOptions<GameStorageContext> options) : base(options)
		{ }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<User>(etb =>
			{
				etb.HasKey(e => e.Id);
				etb.HasIndex(e => e.Name);
				etb.Property(e => e.Id).ValueGeneratedOnAdd();
				etb.Property(e => e.Name).IsRequired();
				etb.ToTable("Users");
			});

			modelBuilder.Entity<Pet>(etb =>
			{
				etb.HasKey(e => e.Id);
				etb.HasIndex(e => e.Name);
				etb.Property(e => e.Id).ValueGeneratedOnAdd();
				etb.Property(e => e.Name).IsRequired();
				etb.ToTable("Pets");
			});

			modelBuilder.Entity<Pet>().Ignore(e => e.Effects);

			modelBuilder.Entity<Pet>().HasOne(e => e.User).WithMany(t => t.Pets).HasForeignKey(e => e.UserId);

			modelBuilder.Entity<UserProduct>().HasKey(o => new { o.UserId, o.ProductId });

			modelBuilder.Entity<UserProduct>()
			 .HasOne(u => u.User)
			 .WithMany(p => p.UserProducts)
			 .HasForeignKey(u => u.UserId);
		}

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			base.OnConfiguring(optionsBuilder);

			// Uncomment if you want to make new migrations manually.
			optionsBuilder.UseSqlServer("Server = (localdb)\\mssqllocaldb; Database = Tamagotchi; Trusted_Connection = True;");
		}
	}
}
