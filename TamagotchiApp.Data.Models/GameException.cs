﻿using System;
using TamagotchiApp.Data.Models.Entities;

namespace TamagotchiApp.Data.Models
{
	[Serializable]
	public class GameException: Exception
	{
		public GameException(string message) : base(message)
		{
		}
	}
}
