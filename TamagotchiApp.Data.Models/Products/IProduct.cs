﻿using System;
using System.Collections.Generic;
using System.Text;
using TamagotchApp.Data.Models.Effects;

namespace TamagotchApp.Data.Models.Products
{
	public interface IProduct : IEffect
	{
		public int Id { get; set; }

		public decimal Cost { get; set; }

		public string Name { get; set; }
	}
}
